import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

private val CoroutineScope.name get() = Thread.currentThread().name

fun main() {
    with(GlobalScope) {
        launch(Dispatchers.IO) {
            println(name)
        }

        launch(Dispatchers.Default) {
            println(name)
        }

        launch(Dispatchers.Unconfined) {
            println(name)
        }
    }

    Thread.sleep(200)
}